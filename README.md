# rpsc-crimea-vk

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

---
### Run Google Chrome with localhost service worker support (Mac OS)
```
open /Applications/Google\ Chrome.app --args --allow-insecure-localhost
```