import Vue from 'vue'
import 'babel-polyfill'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'
import VueHtml2Canvas from 'vue-html2canvas'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './registerServiceWorker'

Vue.use(BootstrapVue)
Vue.use(VueHtml2Canvas)

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
