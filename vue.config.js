const fs = require('fs')
// const CompressionWebpackPlugin = require('compression-webpack-plugin')
// const productionGzipExtensions = ['js', 'css']

let baseUrl = ''
let devServer = {}
if (process.env.SNAME) {
  baseUrl = 'https://sukochev.name/rpsc-crimea-vk/'
} else if (process.env.NODE_ENV === 'production') {
  baseUrl = 'https://sukochev.gitlab.io/rpsc-crimea-vk/'
}

if (process.env.NODE_ENV !== 'production') {
  devServer = {
    https: {
      key: fs.readFileSync('./certs/localhost.key'),
      cert: fs.readFileSync('./certs/localhost.crt')
    }
  }
}

module.exports = {
  baseUrl,
  productionSourceMap: false,
  // configureWebpack: {
  //   plugins: [
  //     new CompressionWebpackPlugin({
  //       filename: '[path].gz[query]',
  //       algorithm: 'gzip',
  //       test: new RegExp('\\.(' + productionGzipExtensions.join('|') + ')$'),
  //       threshold: 10240,
  //       minRatio: 0.8
  //     })
  //   ]
  // },
  pwa: {
    workboxPluginMode: 'GenerateSW',
    name: 'Старообрядцы в Крыму (генератор изображения для VK)'
  },
  devServer
}
